﻿var apartmentPageApp = (function () {
    var appAppartmentListVm = {
        apartments: [],
        meters: [],
        newApartment: {
            Street: "",
            HouseNumber: "",
            ApartmentNumber: "",
            MeterId: 0
        },
        updateApartmentMeter: {
            Id: 0,
            Value: 0,
            NewId: null
        },
        filter: {
            StreetContains: "",
            HouseNumberContains: "",
            ApartmentNumberContains:""
        }
    }
    var appApartmentList = new Vue({
        el: '#apartmentListArea',
        data: appAppartmentListVm,
        methods: {
            add: function () {
                var context = this;
                axios.post('/home/add', appAppartmentListVm.newApartment)
                    .then(function (response) {
                        $("#addApartmentModal").modal('hide');
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            get: function () {
                var query = {
                    StreetContains: appAppartmentListVm.filter.StreetContains,
                    HouseNumberContains: appAppartmentListVm.filter.HouseNumberContains,
                    ApartmentNumberContains: appAppartmentListVm.filter.ApartmentNumberContains
                }
                axios.post('/home/get', appAppartmentListVm.filter)
                    .then(function (response) {
                        appAppartmentListVm.apartments = response.data.Apartments;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            getMeters: function () {
                var query = {
                    Available: true,
                    SerialNumberContains: ""
                }
                axios.post('/meter/get', query)
                    .then(function (response) {
                        appAppartmentListVm.meters = response.data.Meters;
                        console.log(response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            showMeterModal: function(meterId) {
                appAppartmentListVm.updateApartmentMeter.Id = meterId;
                $("#editApartmentMeterModal").modal('show');
            },
            update: function() {
                var context = this;
                axios.post('/home/update', appAppartmentListVm.updateApartmentMeter)
                    .then(function (response) {
                        $("#editApartmentMeterModal").modal('hide');
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            unbind: function(id) {
                var context = this;
                axios.post('/home/unbind', { MeterId: id})
                    .then(function (response) {
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
            del: function(id) {
                var context = this;
                axios.post('/home/delete', { Id: id })
                    .then(function (response) {
                        context.get();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            },
        },
        mounted: function () {
            var context = this;
            this.get();
            $('#editApartmentMeterModal').on('shown.bs.modal',
                function() {
                    context.getMeters();
                });
            $('#addApartmentModal').on('shown.bs.modal',
                function () {
                    context.getMeters();
                });
        }
    });
    return {
        init: function () {

        }
    };
})();