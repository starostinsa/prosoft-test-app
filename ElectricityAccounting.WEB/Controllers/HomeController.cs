﻿using System.Web.Mvc;
using ElectricityAccounting.WEB.Models.MeterDashboard;
using MediatR;

namespace ElectricityAccounting.WEB.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;
        public HomeController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Get(ApartmentsListQuery query)
        {
            if(query == null) query = new ApartmentsListQuery();
            var response = _mediator.Send<ApartmentListResult>(query).GetAwaiter().GetResult();
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Add(ApartmentAddCommand query)
        {
            var result = _mediator.Send(query).GetAwaiter().GetResult();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(UpdateApartmentMeterCommand command)
        {
            var result = _mediator.Send(command).GetAwaiter().GetResult();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Unbind(UnbindMeterCommand command)
        {
            return Json(_mediator.Send(command).GetAwaiter().GetResult(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(DeleteApartmentCommand command)
        {
            return Json(_mediator.Send(command).GetAwaiter().GetResult(), JsonRequestBehavior.AllowGet);
        }
    }
}