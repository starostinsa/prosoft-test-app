﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElectricityAccounting.WEB.Models.MeterManage;
using MediatR;

namespace ElectricityAccounting.WEB.Controllers
{
    public class MeterController : Controller
    {
        private readonly IMediator _mediator;

        public MeterController(IMediator mediator)
        {
            _mediator = mediator;
        }
        // GET: Metr
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Get(MeterListQuery query)
        {
            if(query == null) query = new MeterListQuery();
            var response = _mediator.Send<MeterListResult>(query).GetAwaiter().GetResult();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Add(MeterAddCommand meter)
        {
            var response = _mediator.Send(meter).GetAwaiter().GetResult();
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}