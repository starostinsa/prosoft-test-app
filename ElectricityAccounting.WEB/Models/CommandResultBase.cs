﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElectricityAccounting.WEB.Models
{
    public abstract class CommandResultBase
    {
        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}