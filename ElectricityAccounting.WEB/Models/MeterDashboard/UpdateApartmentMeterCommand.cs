﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using ElectricityAccounting.DAL.Database.Models;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterDashboard
{
    public class UpdateApartmentMeterCommand: IRequest<UpdateApartmentMeterResult>
    {
        public int Id { get; set; }
        public int? NewId { get; set; }
        public decimal Value { get; set; }
    }

    public class UpdateApartmentMeterResult : CommandResultBase
    {
        public UpdateApartmentMeterResult() { Ok = true; }
    }
    public class UpdateApartmentMeterHendler : IRequestHandler<UpdateApartmentMeterCommand, UpdateApartmentMeterResult>
    {
        public Task<UpdateApartmentMeterResult> Handle(UpdateApartmentMeterCommand request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                var meter = context.Meters
                    .Include(i => i.MeterValues)
                    .Include(i=>i.HistoryList)
                    .FirstOrDefault(w => w.Id == request.Id);

                if (meter == null)
                    return Task.FromResult(
                        new UpdateApartmentMeterResult() {Ok = false, Message = "Счетчик не найден"});

                if (request.NewId.HasValue && !context.Meters.Any(a => a.Id == request.NewId))
                    return Task.FromResult(
                        new UpdateApartmentMeterResult() { Ok = false, Message = "Счетчик не найден" });

                if (!request.NewId.HasValue && meter.MeterValues.Any() && meter.MeterValues.Max(m=>m.Value) > request.Value)
                    return Task.FromResult(
                        new UpdateApartmentMeterResult() { Ok = false, Message = "Новое показание счетчика не может быть меньше предыдущего" });

                if (request.NewId.HasValue)
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var newMeter = context.Meters
                                .Include(i=>i.MeterValues)
                                .Include(i=>i.HistoryList)
                                .FirstOrDefault(w => w.Id == request.NewId);
                            var apartment = context.Apartments.FirstOrDefault(w => w.Id == request.Id);
                            apartment.Meter = newMeter;
                            newMeter.MeterValues.Clear();
                            meter.HistoryList.Add(new MeterHistory()
                            {
                                MeterAction = MeterAction.Uninstalled,
                                InsertDateTime = DateTime.UtcNow,
                                MeterActionDescription = $"Счетчик был демантирован и заменен на новый {newMeter.SerialNumber}"
                            });
                            newMeter.HistoryList.Add(new MeterHistory()
                            {
                                MeterAction = MeterAction.Installed,
                                InsertDateTime = DateTime.UtcNow,
                                MeterActionDescription = $"Счетчик был установлен взамен {meter.SerialNumber}"
                            });
                            context.SaveChanges();
                            transaction.Commit();
                            return Task.FromResult(new UpdateApartmentMeterResult());
                        }
                        catch (Exception e)
                        {
                            transaction.Rollback();
                            return Task.FromResult(new UpdateApartmentMeterResult() {Ok = false, Message = e.Message});
                        }
                        
                    }
                }

                meter.MeterValues.Add(
                    new MeterValue() {CheckValueDateTime = DateTime.UtcNow, Value = request.Value});
                context.SaveChanges();
                return Task.FromResult(new UpdateApartmentMeterResult());
            }
        }
    }
}