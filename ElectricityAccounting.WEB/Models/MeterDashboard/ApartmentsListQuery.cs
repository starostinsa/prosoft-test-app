﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using ElectricityAccounting.WEB.Helpers;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterDashboard
{
    public class ApartmentsListQuery : PaginationQuery, IRequest<ApartmentListResult>
    {
        public ApartmentsListQuery()
        {
            PageIndex = 1;
            PageSize = 1000;
        }
        // Filters
        public string StreetContains { get; set; }
        public string HouseNumberContains { get; set; }
        public string ApartmentNumberContains { get; set; }
    }

    public class ApartmentListResult : PaginationListQueryResult
    {
        // Base result
        public List<Apartment> Apartments { get; set; }
        // View model
        public class Apartment
        {
            public int Id { get; set; }
            public string Street { get; set; }
            public string HouseNumber { get; set; }
            public string ApartmentNumber { get; set; }
            public string MeterSerialNumber { get; set; }
            public DateTime? MeterLastCheck { get; set; }
            public DateTime? MeterNextCheck { get; set; }
            public decimal MeterValue { get; set; }
            public int MeterId { get; set; }           
        }
    }

    public class ApartmentsListHandler : IRequestHandler<ApartmentsListQuery, ApartmentListResult>
    {
        public Task<ApartmentListResult> Handle(ApartmentsListQuery request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                var query = context.Apartments.Include(i=>i.Meter).OrderBy(o => o.Id).AsQueryable();
                if (!string.IsNullOrEmpty(request.StreetContains))
                    query = query.Where(w => w.Street.Contains(request.StreetContains));
                if (!string.IsNullOrEmpty(request.HouseNumberContains))
                    query = query.Where(w => w.HouseNumber.Contains(request.HouseNumberContains));
                if (!string.IsNullOrEmpty(request.ApartmentNumberContains))
                    query = query.Where(w => w.ApartmentNumber.Contains(request.ApartmentNumberContains));

                var queryResult = PaginatedList<ApartmentListResult.Apartment>
                    .Create(
                        query.Select(s => new ApartmentListResult.Apartment()
                        {
                            Id = s.Id,
                            Street = s.Street,
                            HouseNumber = s.HouseNumber,
                            ApartmentNumber = s.ApartmentNumber,
                            MeterLastCheck = s.Meter.LastCheck,
                            MeterNextCheck = s.Meter.NextCheck,
                            MeterSerialNumber = s.Meter.SerialNumber,
                            MeterValue = s.Meter.MeterValues.Any() ? s.Meter.MeterValues.OrderByDescending(o => o.CheckValueDateTime).FirstOrDefault().Value : 0,
                            MeterId = s.Meter.Id
                        }),
                        request.PageIndex,
                        request.PageSize
                    );

                return Task.FromResult(new ApartmentListResult()
                {
                    Apartments = queryResult,
                    Count = queryResult.TotalCount,
                    PageIndex = queryResult.PageIndex,
                    PageSize = request.PageSize
                });
            }
        }
    }
}