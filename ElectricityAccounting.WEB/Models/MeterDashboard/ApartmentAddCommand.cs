﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using ElectricityAccounting.DAL.Database.Models;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterDashboard
{
    public class ApartmentAddCommand : IRequest<ApartmentAddCommandResult>
    {
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public int MeterId { get; set; }
    }

    public class ApartmentAddCommandResult : CommandResultBase { }

    public class ApartmentAddCommandHandler : IRequestHandler<ApartmentAddCommand, ApartmentAddCommandResult>
    {
        public Task<ApartmentAddCommandResult> Handle(ApartmentAddCommand request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                if (string.IsNullOrEmpty(request.Street))
                    return Task.FromResult(new ApartmentAddCommandResult() { Ok = false, Message = "Укажите наименование улицы" });
                if (string.IsNullOrEmpty(request.HouseNumber))
                    return Task.FromResult(new ApartmentAddCommandResult() { Ok = false, Message = "Укажите номер дома" });
                if (string.IsNullOrEmpty(request.ApartmentNumber))
                    return Task.FromResult(new ApartmentAddCommandResult() { Ok = false, Message = "Укажите номер квартиры" });
                if (context.Meters.FirstOrDefault(w=>w.Id == request.MeterId) == null)
                    return Task.FromResult(new ApartmentAddCommandResult() { Ok = false, Message = "Счетчик не найден" });
                if (context.Apartments
                    .Any(a => a.Street.ToUpper() == request.Street.ToUpper() &&
                              a.HouseNumber.ToUpper() == request.HouseNumber.ToUpper() &&
                              a.ApartmentNumber.ToUpper() == request.ApartmentNumber.ToUpper()))
                    return Task.FromResult(new ApartmentAddCommandResult() {Message = "Квартира уже добавлена"});

                var apartment = new Apartment()
                {
                    Street = request.Street,
                    HouseNumber = request.HouseNumber,
                    ApartmentNumber = request.ApartmentNumber,
                    Meter = context.Meters.First(w=>w.Id == request.MeterId)
                };
                context.Apartments.Add(apartment);
                context.SaveChanges();
                return Task.FromResult(new ApartmentAddCommandResult() {Ok = true});
            }
        }
    }
}