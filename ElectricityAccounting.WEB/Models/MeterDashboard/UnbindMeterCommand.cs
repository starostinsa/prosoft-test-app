﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterDashboard
{
    public class UnbindMeterCommand:IRequest<UnbindMeterCommandResult>
    {
        public int MeterId { get; set; }
    }

    public class UnbindMeterCommandResult : CommandResultBase
    {
        public UnbindMeterCommandResult() { Ok = true; }
    }

    public class UnbindMeterCommandHandler : IRequestHandler<UnbindMeterCommand, UnbindMeterCommandResult>
    {
        public Task<UnbindMeterCommandResult> Handle(UnbindMeterCommand request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                if (!context.Meters.Any(a=>a.Id==request.MeterId))
                    return Task.FromResult(new UnbindMeterCommandResult(){Ok = false, Message = "Счетчик не найден"});
                if (!context.Apartments.Any(a => a.MeterId == request.MeterId))
                    return Task.FromResult(new UnbindMeterCommandResult());
                var apartment = context.Apartments.FirstOrDefault(w => w.MeterId == request.MeterId);
                apartment.Meter = null;
                context.SaveChanges();
                return Task.FromResult(new UnbindMeterCommandResult());
            }
        }
    }
}