﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterDashboard
{
    public class DeleteApartmentCommand:IRequest<DeleteApartmentCommandResult>
    {
        public int Id { get; set; }
    }

    public class DeleteApartmentCommandResult : CommandResultBase
    {
        public DeleteApartmentCommandResult()
        {
            Ok = true;
        }
    }

    public class DeleteApartmentCommandHandler : IRequestHandler<DeleteApartmentCommand, DeleteApartmentCommandResult>
    {
        public Task<DeleteApartmentCommandResult> Handle(DeleteApartmentCommand request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                if (!context.Apartments.Any(a => a.Id == request.Id))
                    return Task.FromResult(new DeleteApartmentCommandResult());
                context.Apartments.Remove(context.Apartments.FirstOrDefault(w => w.Id == request.Id));
                context.SaveChanges();
                return Task.FromResult(new DeleteApartmentCommandResult());
            }
        }
    }
}