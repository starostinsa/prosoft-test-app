﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ElectricityAccounting.DAL.Database;
using ElectricityAccounting.DAL.Database.Models;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterManage
{
    public class MeterAddCommand : IRequest<MeterAddResult>
    {
        public string SerialNumber { get; set; }
    }
    public class MeterAddResult:CommandResultBase
    {
        public MeterAddResult() { Ok = true; }
    }
    public class MeterAddHandler : IRequestHandler<MeterAddCommand, MeterAddResult>
    {
        public Task<MeterAddResult> Handle(MeterAddCommand request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                if (string.IsNullOrEmpty(request.SerialNumber))
                    return Task.FromResult(new MeterAddResult()
                    {
                        Ok = false,
                        Message = "Укажите серийный номер"
                    });
                if (context.Meters.Any() && context.Meters.Any(a => a.SerialNumber.ToUpper() == request.SerialNumber.ToUpper()))
                    return Task.FromResult(new MeterAddResult()
                    {
                        Ok = false,
                        Message = "Счетчик с таким серийным номером уже добавлен в систему"
                    });
                context.Meters.Add(new Meter() { SerialNumber = request.SerialNumber.ToUpper(), InsertTime = DateTime.UtcNow });
                context.SaveChanges();
                return Task.FromResult(new MeterAddResult());
            }
        }
    }
}