﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using ElectricityAccounting.DAL.Database;
using ElectricityAccounting.WEB.Helpers;
using MediatR;

namespace ElectricityAccounting.WEB.Models.MeterManage
{
    public class MeterListQuery : PaginationQuery, IRequest<MeterListResult>
    {
        public MeterListQuery()
        {
            PageIndex = 1;
            PageSize = 1000;
        }
        public bool? Available { get; set; }
        public string SerialNumberContains { get; set; }
    }

    public class MeterListResult : PaginationListQueryResult
    {
        public MeterListResult()
        {
            PageIndex = 1;
            PageSize = 1000;
        }
        public List<Meter> Meters { get; set; }
        public class Meter
        {
            public int Id { get; set; }
            public string SerialNumber { get; set; }
            public DateTime? LastCheck { get; set; }
            public DateTime? NextCheck { get; set; }
            public decimal Value { get; set; }
        }
    }

    public class MeterListHandler : IRequestHandler<MeterListQuery, MeterListResult>
    {
        public Task<MeterListResult> Handle(MeterListQuery request, CancellationToken cancellationToken)
        {
            using (var context = new ElectricityAccountingContext())
            {
                var query = context.Meters.OrderByDescending(o => o.Id).AsQueryable();
                if (!string.IsNullOrEmpty(request.SerialNumberContains))
                    query = query.Where(w => w.SerialNumber.ToUpper().Contains(request.SerialNumberContains.ToUpper()));
                if (request.Available.HasValue)
                {
                    var avMetersId = context.Apartments.Select(s => s.Meter.Id).AsQueryable();
                    query = request.Available.Value
                        ? query.Where(w => !avMetersId.Contains(w.Id))
                        : query.Where(w => avMetersId.Contains(w.Id));
                }

                var queryResult = PaginatedList<MeterListResult.Meter>.Create(query.Select(s =>
                        new MeterListResult.Meter()
                        {
                            Id = s.Id,
                            LastCheck = s.LastCheck,
                            NextCheck = s.NextCheck,
                            SerialNumber = s.SerialNumber,
                            Value = s.MeterValues.Any() ? s.MeterValues.OrderByDescending(o => o.Value).FirstOrDefault().Value : 0
                        }),
                    request.PageIndex,
                    request.PageSize);
                return Task.FromResult(new MeterListResult()
                {
                    Meters = queryResult,
                    Count = queryResult.TotalCount,
                    PageIndex = queryResult.PageIndex,
                    PageSize = request.PageSize
                });
            }
        }
    }
}