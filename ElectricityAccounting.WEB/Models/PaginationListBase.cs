﻿using MediatR;

namespace ElectricityAccounting.WEB.Models
{
    public abstract class PaginationQuery : IRequest<PaginationListQueryResult>
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
    public abstract class PaginationListQueryResult
    {
        public int Count { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}