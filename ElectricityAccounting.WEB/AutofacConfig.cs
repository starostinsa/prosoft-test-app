﻿using System.Reflection;
using Autofac;
using ElectricityAccounting.DAL.Database;
using MediatR;
using MediatR.Pipeline;
using Ping = ElectricityAccounting.WEB.CQRS.Ping;

namespace ElectricityAccounting.WEB
{
    public class AutofacConfig
    {
        private static IMediator BuildMediator()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(typeof(IMediator).GetTypeInfo().Assembly).AsImplementedInterfaces();
            builder.RegisterType<ElectricityAccountingContext>()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();

            var mediatrOpenTypes = new[]
            {
                typeof(IRequestHandler<,>),
                typeof(INotificationHandler<>),
            };

            foreach (var mediatrOpenType in mediatrOpenTypes)
            {
                builder
                    .RegisterAssemblyTypes(typeof(Ping).GetTypeInfo().Assembly)
                    .AsClosedTypesOf(mediatrOpenType)
                    .AsImplementedInterfaces();
            }


            // It appears Autofac returns the last registered types first
            builder.RegisterGeneric(typeof(RequestPostProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));
            builder.RegisterGeneric(typeof(RequestPreProcessorBehavior<,>)).As(typeof(IPipelineBehavior<,>));

            builder.Register<ServiceFactory>(ctx =>
            {
                var c = ctx.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            var container = builder.Build();

            // The below returns:
            //  - RequestPreProcessorBehavior
            //  - RequestPostProcessorBehavior
            //  - GenericPipelineBehavior

            //var behaviors = container
            //    .Resolve<IEnumerable<IPipelineBehavior<Ping, Pong>>>()
            //    .ToList();

            var mediator = container.Resolve<IMediator>();

            return mediator;
        }
        //public static void ConfigureContainer()
        //{
        //    // получаем экземпляр контейнера
        //    var builder = new ContainerBuilder();

        //    // регистрируем контроллер в текущей сборке
        //    builder.RegisterControllers(typeof(MvcApplication).Assembly);

        //    // регистрируем споставление типов
        //    // builder.RegisterType<BookRepository>().As<IRepository>();

        //    // создаем новый контейнер с теми зависимостями, которые определены выше
        //    var container = builder.Build();

        //    // установка сопоставителя зависимостей
        //    DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        //}
    }
}