﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityAccounting.DAL.Database.Models
{
    public class MeterHistory
    {
        public int Id { get; set; }
        public MeterAction MeterAction { get; set; }
        public string MeterActionDescription { get; set; }
        public string Comment { get; set; }
        public DateTime InsertDateTime { get; set; }
    }

    public enum MeterAction
    {
        [Description("Создан новый счетчик в системе")] Create,
        [Description("Счетчик установлен в квартиру")] Installed,
        [Description("Счетчик был демонтирован из квартиры")] Uninstalled
    }
}
