﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityAccounting.DAL.Database.Models
{
    public class MeterValue
    {
        public int Id { get; set; }
        public decimal Value { get; set; }
        public DateTime CheckValueDateTime { get; set; }
    }
}
