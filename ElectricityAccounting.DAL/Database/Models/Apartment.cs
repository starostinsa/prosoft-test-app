﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityAccounting.DAL.Database.Models
{
    public class Apartment
    {
        [Key]
        public int Id { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string ApartmentNumber { get; set; }
        [Required]
        public int MeterId { get; set; }
        public Meter Meter { get; set; }
    }
}
