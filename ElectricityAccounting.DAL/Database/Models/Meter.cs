﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectricityAccounting.DAL.Database.Models
{
    public class Meter
    {
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? LastCheck { get; set; }
        public DateTime? NextCheck { get; set; }
        public TimeSpan CheckTimeSpan { get; set; }
        public List<MeterHistory> HistoryList { get; set; }
        public List<MeterValue> MeterValues { get; set; }
        public DateTime InsertTime { get; set; }
        //public Apartment Apartment { get; set; }
        //public ICollection<Apartment> Apartments { get; set; }
    }
}
