﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectricityAccounting.DAL.Database.Models;

namespace ElectricityAccounting.DAL.Database
{
    public class ElectricityAccountingContext : DbContext
    {
        public ElectricityAccountingContext() : base("DefaultContext")
        {

        }
        public DbSet<Apartment> Apartments { get; set; }
        public DbSet<Meter> Meters { get; set; }
        public DbSet<MeterValue> MeterValues { get; set; }
        public DbSet<MeterHistory> MeterLogs { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Apartment>()
            //    .HasOptional(o => o.Meter);

            base.OnModelCreating(modelBuilder);
        }
    }
}
